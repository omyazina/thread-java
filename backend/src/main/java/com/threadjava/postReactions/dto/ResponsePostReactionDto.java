package com.threadjava.postReactions.dto;

import lombok.Builder;
import lombok.Data;
import java.util.UUID;

@Data
@Builder
public class ResponsePostReactionDto {
    private UUID id;
    private UUID postId;
    private Boolean isLike;
    private UUID userId;
    private long postLikeCount;
    private long postUserLikes;
    private long postDislikeCount;
    private long postUserDislikes;
//    private UUID authorId;
}
